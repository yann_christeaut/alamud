from .action import Action2
from mud.events import AssembleWithEvent
from .action import Action1
from .action import Action3

class AssembleWithAction(Action3):
	EVENT = AssembleWithEvent
	ACTION = "assemblage"
	RESOLVE_OBJECT = "resolve_for_use"
	RESOLVE_OBJECT2 = "resolve_for_use"
