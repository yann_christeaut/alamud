# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect3
from mud.events import PopEvent

class PopEffect(Effect3):
	EVENT = PopEvent

	def make_event(self):
		#~ print(type(self.object),type(self.actor),type(self.yaml["modifs"]))
		return self.EVENT(self.actor, self.object2, self.yaml["modifs"])
