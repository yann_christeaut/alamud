# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3


class PopEvent(Event3):
	NAME = "pop"

	def perform(self):
		self.actor.remove(self.object)
		self.inform("pop")
